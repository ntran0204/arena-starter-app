// @flow
import type { Action } from '../types'
import type { StoreSettingData } from '../types/store_setting'
import { CHANGE_STORE_SETTING } from '../types/store_setting'

const store_setting = (state: StoreSettingData = {}, action: Action): StoreSettingData => {
  switch (action.type) {
    case CHANGE_STORE_SETTING:
      return action.payload

    default:
      return state
  }
}

export default store_setting
