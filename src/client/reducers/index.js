// @flow
import { combineReducers } from 'redux'
import notification from './notification'
import store_setting from './store_setting'

export default combineReducers({
  notification,
  store_setting,
  // another_state_prop,
})
