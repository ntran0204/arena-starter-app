// @flow
import apiCaller from '../utils/apiCaller';

const find = async ({}): Object => {
  return await apiCaller(`/admin/orders`);
};

const OrderApi = {
  find,
};

export default OrderApi;
