// @flow
import apiCaller from '../utils/apiCaller'

const get = async (): Object => {
  return await apiCaller(`admin/store-setting`)
}

const update = async ({ accepted_date }: Object): Object => {
  return await apiCaller(`/admin/store-setting`, 'PUT', { accepted_date })
}

const StoreSettingApi = {
  get,
  update,
}

export default StoreSettingApi
