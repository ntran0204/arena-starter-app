// @flow
import apiCaller from '../utils/apiCaller'

const find = async ({ id, limit, nextPageInfo, previousPageInfo, search, vendor }): Object => {
  let _id = id ? `&id=${id}` : ''
  let _limit = limit ? `&limit=${limit}` : ''
  let _nextPageInfo = nextPageInfo ? `&nextPageInfo=${nextPageInfo}` : ''
  let _previousPageInfo = previousPageInfo ? `&previousPageInfo=${previousPageInfo}` : ''
  let _search = search ? `&search=${search}` : ''
  let _vendor = vendor ? `&vendor=${vendor}` : ''

  return await apiCaller(
    `/admin/graphql-products?${_id}${_limit}${_nextPageInfo}${_previousPageInfo}${_search}${_vendor}`,
  )
}

const ProductGraphqlApi = {
  find,
}

export default ProductGraphqlApi
