// @flow
import * as NotificationAction from './notification'
import * as StoreSettingAction from './store_setting'

const Actions = {
  ...NotificationAction,
  ...StoreSettingAction,
  // ...AnotherAction,
}

export default Actions
