// @flow
import type { NotificationAction } from '../types/notification'
import { SHOW_NOTIFICATION_ACTION } from '../types/notification'

export const showNotification = (payload: Object): NotificationAction => {
  return {
    type: SHOW_NOTIFICATION_ACTION,
    payload,
  }
}
