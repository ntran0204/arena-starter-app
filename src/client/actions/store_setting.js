// @flow
import type { StoreSettingAction } from '../types/store_setting'
import { CHANGE_STORE_SETTING } from '../types/store_setting'

export const changeStoreSetting = (payload: Object): StoreSettingAction => {
  return {
    type: CHANGE_STORE_SETTING,
    payload,
  }
}
