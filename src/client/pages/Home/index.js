import { DisplayText, Stack } from '@shopify/polaris';
import React, { Component } from 'react';
import LoadingPage from './../../components/LoadingPage';

type Props = {};

type State = { isLoading: boolean };

const INITIAL_STATE = {
  isLoading: false,
};

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = INITIAL_STATE;
  }

  render() {
    const { isLoading } = this.state;
    return (
      <Stack vertical>
        {isLoading && <LoadingPage />}
        <DisplayText size="small">Home Page 123</DisplayText>
      </Stack>
    );
  }
}

export default Home;
