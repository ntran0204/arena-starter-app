// @flow
import { Modal, Stack, TextField } from '@shopify/polaris'
import React, { Component } from 'react'

type Props = {
  product: Object,
  onClose: () => void,
  onCancel: () => void,
  onSubmit: (data: Object) => void,
  onChange: (data: Object) => void,
}

type State = {
  product: Object,
}

const INITIAL_STATE = {
  product: null,
}

class CreateForm extends Component {
  constructor(props) {
    super(props)
    this.state = INITIAL_STATE
  }

  static getDerivedStateFromProps(props, state) {
    if (!state.product) {
      return { product: props.product }
    }

    return null
  }

  render() {
    const { onClose, onCancel, onSubmit, onChange } = this.props
    const { product } = this.state

    return (
      <Modal
        open={true}
        onClose={onClose}
        title="Reach more shoppers with Instagram product tags"
        primaryAction={{
          content: product.id ? 'Save' : 'Add',
          onAction: () => onSubmit(product),
        }}
        secondaryActions={[
          {
            content: product.id ? 'Cancel' : 'Discard',
            onAction: onCancel,
          },
        ]}
      >
        <Modal.Section>
          <Stack vertical alignment="fill">
            <TextField
              label="Title"
              placeholder=""
              value={product.title.value}
              error={product.title.errMsg}
              onChange={(value) => {
                let _product = JSON.parse(JSON.stringify(product))
                _product.title = { value, errMsg: '' }
                this.setState({ product: _product })
              }}
            />
            <TextField
              label="Body_html"
              placeholder=""
              value={product.body_html.value}
              error={product.body_html.errMsg}
              multiline={4}
              onChange={(value) => {
                let _product = JSON.parse(JSON.stringify(product))
                _product.body_html = { value, errMsg: '' }
                this.setState({ product: _product })
              }}
            />
          </Stack>
        </Modal.Section>
      </Modal>
    )
  }
}

export default CreateForm
