import {
  ActionList,
  Button,
  Card,
  DataTable,
  DisplayText,
  Pagination,
  Popover,
  Stack,
} from '@shopify/polaris';
import React, { Component } from 'react';
import OrderApi from '../../apis/order';
import LoadingPage from '../../components/LoadingPage';
import dateFormat from 'dateformat';

type Props = {};

type State = {
  isLoading: Boolean,
  limit: Number,
  search: String,
  orders: Object,
};

const INITIAL_STATE = {
  isLoading: false,
  limit: 20,
  search: '',
  orders: {},
};

const LIMITS = [10, 20, 50, 100];

class Orders extends Component {
  constructor(props) {
    super(props);
    this.state = INITIAL_STATE;
  }

  async getOrders() {
    const { actions } = this.props;

    try {
      let res = await OrderApi.find({});
      console.log('getOrders res :>> ', res);
      if (res.success) {
        this.setState({ orders: res.payload });
      } else {
        throw res.error;
      }
    } catch (error) {
      actions.showNotification({ show: true, error: true, content: error.message });
    }
  }

  componentDidMount() {
    this.getOrders();
  }

  render() {
    const { isLoading, limit, openLimits, orders } = this.state;

    console.log('orders :>> ', orders);
    let rows =
      orders?.orders?.map((order, index) => {
        console.log(order);
        let totalQuantity = order.line_items.reduce((acc, lineItem) => acc + lineItem.quantity, 0);
        console.log(totalQuantity);
        return [
          order.name,
          dateFormat(order.created_at, 'mmm, dd at hh:mm TT'),
          order?.customer
            ? `${order.customer.first_name} ${order.customer.last_name}`
            : 'No customer',
          order.total_price,
          order.financial_status,
          order.fulfillment_status ? order.fulfillment_status : 'unfulfilled',
          `${totalQuantity} ${totalQuantity > 1 ? 'items' : 'item'}`,
        ];
      }) || [];

    return (
      <Stack vertical>
        {isLoading && <LoadingPage />}
        <Stack distribution="equalSpacing">
          <Popover
            active={openLimits}
            activator={
              <Button onClick={() => this.setState({ openLimits: !openLimits })} disclosure>
                {limit}
              </Button>
            }
            autofocusTarget="first-node"
            onClose={() => this.setState({ openLimits: false })}
          >
            <ActionList
              actionRole="menuitem"
              items={LIMITS.map((item) => ({
                content: item,
                onAction: () =>
                  this.setState({ limit: item, openLimits: false }, () => this.getOrders({})),
              }))}
            />
          </Popover>
          <Button
            primary
            onClick={() =>
              this.setState({
                product: {
                  title: { value: '', errMsg: '' },
                  body_html: { value: '', errMsg: '' },
                },
              })
            }
          >
            Add order
          </Button>
        </Stack>

        <Card>
          <DataTable
            columnContentTypes={['numeric', 'text', 'text', 'text', 'text', 'text']}
            headings={[
              'Order',
              'Date',
              'Customer',
              'Total',
              'Payment status',
              'Fulfillment status',
              'Items',
            ]}
            rows={rows}
            // footerContent={
            //   orders?.orders
            //     ? orders?.orders?.length > 0
            //       ? (() => (
            //           <Stack distribution="center">
            //             <Pagination
            //               hasPrevious={orders.pageInfo.hasPreviousPage}
            //               onPrevious={() =>
            //                 this.getOrders({ pageInfo: orders.pageInfo.previousPageInfo })
            //               }
            //               hasNext={orders.pageInfo.hasNextPage}
            //               onNext={() => this.getOrders({ pageInfo: orders.pageInfo.nextPageInfo })}
            //             />
            //           </Stack>
            //         ))()
            //       : 'You have no data'
            //     : 'Loading..'
            // }
          />
        </Card>
      </Stack>
    );
  }
}

export default Orders;
