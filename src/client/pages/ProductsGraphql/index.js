// @flow
import {
  Stack,
  Card,
  ResourceList,
  ResourceItem,
  Thumbnail,
  Badge,
  DataTable,
  Button,
  Modal,
  Popover,
  ActionList,
  Pagination,
} from '@shopify/polaris'
import React, { Component } from 'react'
import ProductGraphqlApi from '../../apis/product_graphql'
import LoadingPage from '../../components/LoadingPage'
import { ViewMinor, EditMinor, DeleteMinor } from '@shopify/polaris-icons'
import CreateForm from './CreateForm'

type Props = {}

type State = {
  isLoading: Boolean,
  products: Object,
  product: Object,
  deleted: Object,
  limit: Number,
  openLimits: Boolean,
  search: String,
}

const LIMITS = [10, 20, 50, 100]

const INITIAL_STATE = {
  isLoading: false,
  products: null,
  product: null,
  deleted: null,
  limit: 20,
  openLimits: false,
  search: '',
}

class ProductsGraphql extends Component {
  constructor(props) {
    super(props)
    this.state = INITIAL_STATE
  }

  getProducts = async ({ nextPageInfo, previousPageInfo }) => {
    const { actions } = this.props
    const { limit, search } = this.state

    try {
      this.setState({ isLoading: true })

      let res = await ProductGraphqlApi.find({ limit, search, nextPageInfo, previousPageInfo })
      if (res.success) {
        this.setState({ products: res.payload })
      } else {
        throw res.error
      }
    } catch (error) {
      actions.showNotification({ show: true, error: true, content: error.message })
    } finally {
      this.setState({ isLoading: false })
    }
  }

  componentDidMount() {
    this.getProducts({})
  }

  handleSubmit = async (product: Object) => {
    const { actions } = this.props

    actions.showNotification({ show: true, error: false, content: 'This feature is coming soon' })

    try {
      // let res = null
      // this.setState({ isLoading: true })
      // if (product.id) {
      //   // update
      //   res = await ProductGraphqlApi.update(product)
      // } else {
      //   // create
      //   res = await ProductGraphqlApi.create(product)
      // }
      // if (res.success) {
      //   actions.showNotification({
      //     show: true,
      //     error: false,
      //     content: product.id ? 'Saved' : 'Added',
      //   })
      //   this.getProducts({})
      // } else {
      //   throw res.error
      // }
    } catch (error) {
      actions.showNotification({ show: true, error: true, content: error.message })
    } finally {
      this.setState({ isLoading: false })
    }
  }

  handleDelete = async (deleted: Object) => {
    const { actions } = this.props

    try {
      this.setState({ isLoading: true })

      let res = await ProductGraphqlApi.delete(deleted.id)
      if (res.success) {
        actions.showNotification({ show: true, error: false, content: 'Deleted' })

        this.getProducts({})
      } else {
        throw res.error
      }
    } catch (error) {
      actions.showNotification({ show: true, error: true, content: error.message })
    } finally {
      this.setState({ isLoading: false })
    }
  }

  render() {
    const { isLoading, products, product, deleted, limit, openLimits } = this.state

    console.log('ProductsGraphql this.state :>> ', this.state)

    let rows = []
    if (products?.items?.length) {
      rows = products.items.map((item, index) => [
        index + 1,
        item.title,
        (() => (
          <Badge
            status={
              item.status.toUpperCase() === 'ACTIVE'
                ? 'success'
                : item.status.toUpperCase() === 'ARCHIRED'
                ? ''
                : 'attention'
            }
          >
            {item.status}
          </Badge>
        ))(),
        item.productType,
        item.vendor,
        (() => (
          <Stack spacing="extraTight" wrap={false}>
            <Button icon={ViewMinor} external url={item.onlineStorePreviewUrl} />
            <Button
              icon={EditMinor}
              onClick={() =>
                this.setState({
                  product: {
                    id: item.id,
                    title: { value: item.title, errMsg: '' },
                    description: { value: item.description, errMsg: '' },
                  },
                })
              }
            />
            <Button icon={DeleteMinor} onClick={() => this.setState({ deleted: item })} />
          </Stack>
        ))(),
      ])
    }

    return (
      <Stack vertical>
        {(isLoading || !products) && <LoadingPage />}

        <Stack distribution="equalSpacing">
          <Popover
            active={openLimits}
            activator={
              <Button onClick={() => this.setState({ openLimits: !openLimits })} disclosure>
                {limit}
              </Button>
            }
            autofocusTarget="first-node"
            onClose={() => this.setState({ openLimits: false })}
          >
            <ActionList
              actionRole="menuitem"
              items={LIMITS.map((item) => ({
                content: item,
                onAction: () =>
                  this.setState({ limit: item, openLimits: false }, () => this.getProducts({})),
              }))}
            />
          </Popover>
          <Button
            primary
            onClick={() =>
              this.setState({
                product: {
                  title: { value: '', errMsg: '' },
                  description: { value: '', errMsg: '' },
                },
              })
            }
          >
            Add product
          </Button>
        </Stack>

        <Card>
          <DataTable
            columnContentTypes={['numeric', 'text', 'text', 'text', 'text', 'text']}
            headings={['No.', 'Title', 'Status', 'Type', 'Vendor', 'Actions']}
            rows={rows}
            footerContent={
              products?.items
                ? products?.items?.length > 0
                  ? (() => (
                      <Stack distribution="center">
                        <Pagination
                          hasPrevious={products.pageInfo.hasPreviousPage}
                          onPrevious={() =>
                            this.getProducts({
                              previousPageInfo: products.pageInfo.previousPageInfo,
                            })
                          }
                          hasNext={products.pageInfo.hasNextPage}
                          onNext={() =>
                            this.getProducts({
                              nextPageInfo: products.pageInfo.nextPageInfo,
                            })
                          }
                        />
                      </Stack>
                    ))()
                  : 'You have no data'
                : 'Loading..'
            }
          />
        </Card>

        {Boolean(product) && (
          <CreateForm
            product={product}
            onClose={() => this.setState({ product: null })}
            onCancel={() => this.setState({ product: null })}
            onSubmit={(product) => {
              this.handleSubmit({
                title: product.title.value,
                description: product.description.value,
              })
              this.setState({ product: null })
            }}
          />
        )}

        {Boolean(deleted) && (
          <Modal
            open={true}
            onClose={() => this.setState({ deleted: null })}
            title="Confirm delete product"
            primaryAction={{
              content: 'Delete',
              onAction: () => {
                this.handleDelete(deleted)
                this.setState({ deleted: null })
              },
              destructive: true,
            }}
            secondaryActions={[
              {
                content: 'Discard',
                onAction: () => this.setState({ deleted: null }),
              },
            ]}
          >
            <Modal.Section>
              <p>Are you sure you want to delete the product? This can’t be undone.</p>
            </Modal.Section>
          </Modal>
        )}
      </Stack>
    )
  }
}

export default ProductsGraphql
