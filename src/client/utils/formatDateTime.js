// @flow
import moment from 'moment'

const formatDateTime = (datetime: string, type: string = 'YYYY-MM-DD'): string => {
  const _datetime = new Date(datetime).getTime()

  const newDate = new Date(_datetime)
  const yyyy = newDate.getFullYear()
  const yyyyStr = `${yyyy}`
  const mm = newDate.getMonth() + 1
  const mmStr = `${mm < 10 ? `0` : ``}${mm}`
  const dd = newDate.getDate()
  const ddStr = `${dd < 10 ? `0` : ``}${dd}`

  switch (type) {
    case 'YYYY-MM-DD':
      // 2020-02-26
      return `${yyyyStr}-${mmStr}-${ddStr}`

    case 'MM/DD/YYYY':
      // 02/26/2020
      return `${mmStr}/${ddStr}/${yyyyStr}`

    case 'Month DD, YYYY':
      // December 25, 2020
      return moment(_datetime).format('LL')

    case 'Month DD, YYYY HH:MM:SS':
      // December 29, 2020 12:00 AM
      return moment(_datetime).format('LLL')

    case 'LL':
      // December 25, 2020
      return moment(_datetime).format(type)

    case 'LLL':
      // December 29, 2020 12:00 AM
      return moment(_datetime).format(type)

    default:
      return moment(_datetime).format(type)
  }
}

export default formatDateTime
