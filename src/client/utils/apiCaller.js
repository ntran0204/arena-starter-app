// @flow
import axios from 'axios'
import { getSessionToken } from '@shopify/app-bridge-utils'

const apiCaller = async (
  endpoint: string,
  method?: string = 'GET',
  data?: Object = null,
  headers?: Object = {},
): Object => {
  try {
    // validate params
    let paramObj = { endpoint }
    let paramKeys = Object.keys(paramObj)
    for (let i = 0, leng = paramKeys.length; i < leng; i++) {
      if (!paramObj[paramKeys[i]]) {
        throw { message: 'Field cannot be blank', field: paramKeys[i] }
      }
    }

    let token = await getSessionToken(window.app)

    let axiosConfig = {
      url: endpoint,
      method,
      data,
      headers: {
        ...headers,
        Authorization: `Bearer ${token}`,
      },
    }

    let res = await axios(axiosConfig)
    if (!res.data?.success && res.data?.error?.message === 'INVALID_SHOP_SESSION') {
      console.log(`INVALID SHOP SESSION`)
      console.log(`Re-call api after 1s`)

      return await new Promise((resolve, reject) => {
        setTimeout(async () => {
          token = await getSessionToken(window.app)
          res = await axios(axiosConfig)

          resolve(res.data)
        }, 1000)
      })
    } else {
      return res.data
    }
  } catch (error) {
    return {
      success: false,
      error: error?.response?.data?.message ? error.response.data : error.message,
    }
  }
}

export default apiCaller
