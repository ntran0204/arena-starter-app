// @flow
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Actions from '../../actions'
import AppMain from '../AppMain'
import { withRouter } from 'react-router-dom'

const mapStateToProps = (state) => {
  return {
    notification: state.notification,
    store_setting: state.store_setting,
    app_nav: state.app_nav,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AppMain))
