// @flow
import React, { Component } from 'react';
import StoreSettingApi from '../../apis/store_setting';
import Preloader from '../Preloader';
import Privacy from '../Privacy';
import { Toast, TitleBar } from '@shopify/app-bridge-react';
import Home from '../../pages/Home';
import Products from '../../pages/Products';
import { Page, Stack } from '@shopify/polaris';
import ProductsGraphql from '../../pages/ProductsGraphql';
import Orders from '../../pages/Orders';

type Props = {};

type State = {
  isReady: Boolean,
  showPrivacy: Boolean,
  tabSelected: String,
};

const APP_NAV = [
  { label: 'Home', value: 'home' },
  { label: 'Products (Rest Api)', value: 'products' },
  { label: 'Products (Graphql Api)', value: 'products-graphql' },
  { label: 'Orders', value: 'orders' },
];

const INITIAL_STATE = {
  isReady: false,
  showPrivacy: false,
  tabSelected: APP_NAV[3].value,
};

class AppMain extends Component {
  constructor(props) {
    super(props);
    this.state = INITIAL_STATE;
  }

  static getDerivedStateFromProps(props, state) {
    const { store_setting } = props;

    if (store_setting) {
      return { isReady: true };
    }

    return null;
  }

  getStoreSetting = async () => {
    const { actions } = this.props;

    let res = await StoreSettingApi.get();
    if (res.success) {
      actions.changeStoreSetting(res.payload);
    } else {
      actions.showNotification({ show: true, error: true, content: res.error.message });
    }
  };

  handleAcceptPrivacy = async () => {
    const { actions } = this.props;

    try {
      let res = await StoreSettingApi.update({ accepted_date: Date.now() });
      if (res.success) {
        actions.changeStoreSetting(res.payload);
      } else {
        throw res.error;
      }
    } catch (error) {
      actions.showNotification({ show: true, error: true, content: error.message });
    }
  };

  componentDidMount() {
    this.getStoreSetting();
  }

  handleAcceptPrivacy = async (): void => {
    const { actions } = this.props;

    let res = await StoreSettingApi.update({ accepted_date: Date.now() });
    if (res.success) {
      actions.changeStoreSetting(res.payload);
    } else {
      actions.showNotification({ show: true, error: true, content: res.error.message });
    }
  };

  hideNotify = () => {
    const { actions } = this.props;

    actions.showNotification({ show: false, error: false, content: '' });
  };

  renderMainContent = () => {
    const { tabSelected } = this.state;

    switch (tabSelected) {
      case 'home':
        return <Home {...this.props} />;

      case 'products':
        return <Products {...this.props} />;

      case 'products-graphql':
        return <ProductsGraphql {...this.props} />;

      case 'orders':
        return <Orders {...this.props} />;

      default:
        return <Home {...this.props} />;
    }
  };

  render() {
    const { actions, store_setting, notification, app_nav } = this.props;
    const { isReady, showPrivacy, tabSelected } = this.state;

    // Console log for dev store
    if (window.shopOrigin === 'ntn-starter-shop-01.myshopify.com') {
      console.log('store :>> ', this.props);
    }

    // App Toast
    const notifyToast = notification.show ? (
      <Toast
        error={notification.error}
        content={notification.content}
        onDismiss={() => this.hideNotify()}
        duration={2000}
      />
    ) : null;

    const SECONDARY_ACTIONS = APP_NAV.map((item) => ({
      content: item.label,
      disabled: item.value === tabSelected,
      onAction: () => this.setState({ tabSelected: item.value }),
    }));

    return (
      <div style={{ paddingBottom: '2em' }}>
        <Page>
          {!isReady && <Preloader />}

          {isReady && (
            <Stack vertical>
              {store_setting.status === 'running' && store_setting.accepted_date ? (
                showPrivacy ? (
                  <Privacy
                    onAction={() => this.setState({ showPrivacy: false })}
                    acceptedDate={store_setting.accepted_date}
                  />
                ) : (
                  <Stack vertical>
                    <TitleBar
                      title="ArenaCommerce WebApp"
                      primaryAction={{
                        content: 'Terms Of Service',
                        disabled: showPrivacy,
                        onAction: () => this.setState({ showPrivacy: true }),
                      }}
                      secondaryActions={SECONDARY_ACTIONS}
                    />

                    {this.renderMainContent()}
                  </Stack>
                )
              ) : (
                <Privacy
                  onAction={() => this.handleAcceptPrivacy()}
                  acceptedDate={store_setting.accepted_date}
                />
              )}
            </Stack>
          )}
        </Page>

        {notifyToast}
      </div>
    );
  }
}

export default AppMain;
