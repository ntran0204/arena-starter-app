// @flow
import React, { Component } from 'react'
// import { hot } from 'react-hot-loader'
import AppContainer from '../AppContainer'
import '@shopify/polaris/dist/styles.css'

class App extends Component {
  render() {
    return <AppContainer />
  }
}

// export default hot(module)(App)
export default App
