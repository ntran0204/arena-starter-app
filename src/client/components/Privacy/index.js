// @flow
import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import ReactMd from 'react-md-file'
import mdFile from './privacy.md'
import { Button, Card, Stack } from '@shopify/polaris'
import './github-markdown.css'
import formatDateTime from '../../utils/formatDateTime'

Privacy.propTypes = {
  onAction: PropTypes.func,
  acceptedDate: PropTypes.string,
}

Privacy.defaultProps = {
  onAction: (): void => null,
}

function Privacy(props) {
  const { onAction, acceptedDate } = props

  useEffect(() => {
    if (document.getElementById('no-root')) {
      document.getElementById('no-root').remove()
    }
  }, [])

  return (
    <div className="app-privacy">
      <Card sectioned>
        <Stack vertical spacing="extraLoose">
          {acceptedDate && (
            <Stack distribution="trailing">
              <p style={{ fontSize: '1.2em' }}>
                Accepted date: <b>{formatDateTime(acceptedDate, 'LLL')}</b>
              </p>
            </Stack>
          )}

          <Stack distribution="trailing">
            <Button primary onClick={onAction}>
              Accept Privacy
            </Button>
          </Stack>

          <div className="privacy-wrapper markdown-body">
            <ReactMd fileName={mdFile} />
          </div>

          <Stack distribution="trailing">
            <Button primary onClick={onAction}>
              Accept Privacy
            </Button>
          </Stack>
        </Stack>
      </Card>
    </div>
  )
}

export default Privacy
