import React from 'react'
import PropTypes from 'prop-types'
import './styles.scss'
import { DisplayText, ProgressBar, Spinner } from '@shopify/polaris'

LoadingPage.propTypes = {
  progress: PropTypes.number,
  label: PropTypes.string,
  onComplete: PropTypes.func,
}

LoadingPage.defaultProps = {
  progress: -1,
  label: '',
  onComplete: (): void => null,
}

function LoadingPage(props) {
  const { progress, label, onComplete } = props

  if (progress >= 100) {
    if (onComplete && typeof onComplete === 'function') {
      setTimeout(() => {
        onComplete()
      }, 1000)
    }
  }

  return (
    <div className={`arena-loading-page${progress >= 0 ? ` arena-loading-page-progress` : ``}`}>
      <div className="loading-page-spinner">
        <Spinner size="small" color="teal" />
        {Boolean(label) && <DisplayText size="medium">{label}</DisplayText>}
      </div>
      {progress >= 0 && <ProgressBar progress={progress} />}
    </div>
  )
}

export default LoadingPage
