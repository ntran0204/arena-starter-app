const ResponseHandler = require('./responseHandler')
const { GraphQLClient, gql } = require('graphql-request')
const { API_VER } = process.env

/**
 *
 * @param {String} shop
 * @param {String} accessToken
 * @param {String} query
 * @returns Object
 */
const graphqlCaller = async (shop, accessToken, query = '') => {
  try {
    // validate params
    let params = { shop, accessToken, query }
    let keys = Object.keys(params)
    for (let i = 0, leng = keys.length; i < leng; i++) {
      if (!params[keys[i]]) {
        throw { message: 'Field cannot be blank', field: paramKeys[i] }
      }
    }

    const graphQLClient = new GraphQLClient(`https://${shop}/admin/api/${API_VER}/graphql.json`, {
      headers: {
        'X-Shopify-Access-Token': accessToken,
        'Content-type': 'application/json',
      },
    })

    return new Promise((resolve, reject) => {
      graphQLClient
        .request(
          gql`
            ${query}
          `,
        )
        .then((data) => {
          resolve(ResponseHandler.success({ data }))
        })
        .catch((error) => {
          let message = ''
          let code = ''
          let cost = null
          let deplay = 0

          if (
            error.response &&
            error.response.errors &&
            error.response.errors[0] &&
            error.response.errors[0].message
          ) {
            message = error.response.errors[0].message

            if (
              error.response.errors[0].extensions &&
              error.response.errors[0].extensions.code === 'THROTTLED'
            ) {
              code = error.response.errors[0].extensions.code
            }
          } else {
            message = error.message
          }

          // handle Throttled cost
          if (code === 'THROTTLED') {
            cost = error.response.extensions.cost
            delay = (cost.requestedQueryCost - cost.throttleStatus.currentlyAvailable) / 50 + 2

            console.log(`graphqlCaller Throttled --> delay ${Math.ceil(delay)}s`)

            // re-call api
            setTimeout(() => {
              graphQLClient
                .request(
                  gql`
                    ${query}
                  `,
                )
                .then((data) => {
                  resolve(ResponseHandler.success({ data }))
                })
                .catch((error) => {
                  message = ''

                  if (
                    error.response &&
                    error.response.errors &&
                    error.response.errors[0] &&
                    error.response.errors[0].message
                  ) {
                    message = error.response.errors[0].message
                  } else {
                    message = error.message
                  }

                  resolve(ResponseHandler.error({ message }))
                })
            }, delay * 1000)
          } else {
            resolve(ResponseHandler.error({ message }))
          }
        })
    })
  } catch (error) {
    return ResponseHandler.error({ message: error.message })
  }
}

module.exports = graphqlCaller
