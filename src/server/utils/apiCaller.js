const axios = require('axios')
const ResponseHandler = require('./responseHandler')

/**
 *
 * @param {String} link
 * @returns Object
 */
const getPageInfo = (link) => {
  let pageInfo = {
    hasNextPage: false,
    hasPreviousPage: false,
    nextPageInfo: '',
    previousPageInfo: '',
  }

  if (link) {
    if (link.indexOf('>; rel="previous"') >= 0) {
      pageInfo.hasPreviousPage = true
      pageInfo.previousPageInfo = link.slice(
        link.indexOf('page_info=') + 'page_info='.length,
        link.indexOf('>; rel="previous"'),
      )
    }

    if (link.indexOf('>; rel="next"') >= 0) {
      pageInfo.hasNextPage = true
      pageInfo.nextPageInfo = link.slice(
        link.lastIndexOf('page_info=') + 'page_info='.length,
        link.indexOf('>; rel="next"'),
      )
    }
  }

  return pageInfo
}

/**
 *
 * @param {shop: String, accessToken: String, method: String, data: Object, headers: Object, pageInfo: Boolean} param0
 * @returns Object
 */
const apiCaller = async ({ shop, accessToken, endpoint, method, data, headers, pageInfo }) => {
  try {
    // validate params
    let paramObj = { shop, accessToken, endpoint }
    let paramKeys = Object.keys(paramObj)
    for (let i = 0, leng = paramKeys.length; i < leng; i++) {
      if (!paramObj[paramKeys[i]]) {
        throw { message: 'Field cannot be blank', field: paramKeys[i] }
      }
    }

    const res = await axios({
      url: `https://${shop}${endpoint}`,
      method: method || 'GET',
      headers: {
        ...(headers || {}),
        'X-Shopify-Access-Token': accessToken,
        'Content-type': 'application/json; charset=utf-8',
      },
      data,
    })

    let payload = res.data

    if (pageInfo) {
      payload.pageInfo = getPageInfo(res.headers.link)
    }

    return ResponseHandler.success(payload)
  } catch (error) {
    let _error = error

    if (_error.response && _error.response.data) {
      _error = _error.response.data
    }

    console.log(`apiCaller error`, _error)
    return ResponseHandler.error(_error)
  }
}

module.exports.apiCaller = apiCaller
