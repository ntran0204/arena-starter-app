const Router = require('koa-router');
const AdminApi = new Router();
const bodyParser = require('koa-bodyparser');
const AppStatus = require('../middlewares/app-status');
const verifyToken = require('./verify-token');
const StoreSettingMiddleware = require('../middlewares/store_setting');
const ProductMiddleware = require('../middlewares/product');
const ProductGraphqlMiddleware = require('../middlewares/product_graphql');
const OrderMiddleware = require('../middlewares/order');

const getAppToken = async (ctx, next) => {
  try {
    const storeInfo = await AppStatus.GetStore(ctx.state.shop);
    if (storeInfo.status === 'error') {
      throw new Error(storeInfo.msg);
    }

    if (storeInfo.store.status !== 'installed' && storeInfo.store.status !== 'running') {
      throw new Error('App still not installed');
    }

    if (!storeInfo.store.token) {
      throw new Error('Invalid app token');
    }

    ctx.state.accessToken = storeInfo.store.token;
    return next();
  } catch (err) {
    ctx.body = {
      status: 'error',
      msg: err.message,
    };
    return;
  }
};

AdminApi.use(verifyToken);
AdminApi.use(getAppToken);

/**
 * ================================================
 * STORE SETTINGS
 */
AdminApi.get('/store-setting', async (ctx) => {
  const { shop, accessToken } = ctx.state;
  ctx.body = await StoreSettingMiddleware.find({ shop, accessToken });
});

AdminApi.put('/store-setting', bodyParser(), async (ctx) => {
  const { shop, accessToken } = ctx.state;
  const { accepted_date } = ctx.request.body;
  ctx.body = await StoreSettingMiddleware.update({ shop, accessToken, accepted_date });
});
/**
 * ================================================
 */

/**
 * ================================================
 * PRODUCTS
 */
AdminApi.get('/products', async (ctx) => {
  const { shop, accessToken } = ctx.state;
  const { limit, pageInfo, search } = ctx.request.query;
  ctx.body = await ProductMiddleware.find({ shop, accessToken, limit, pageInfo, search });
});

AdminApi.get('/products/:id', async (ctx) => {
  const { shop, accessToken } = ctx.state;
  const { id } = ctx.params;
  ctx.body = await ProductMiddleware.findById({ shop, accessToken, id });
});

AdminApi.get('/products-count', async (ctx) => {
  const { shop, accessToken } = ctx.state;
  ctx.body = await ProductMiddleware.getCount({ shop, accessToken });
});

AdminApi.post('/products', bodyParser(), async (ctx) => {
  const { shop, accessToken } = ctx.state;
  const { product } = ctx.request.body;
  ctx.body = await ProductMiddleware.create({ shop, accessToken, product });
});

AdminApi.put('/products', bodyParser(), async (ctx) => {
  const { shop, accessToken } = ctx.state;
  const { product } = ctx.request.body;
  ctx.body = await ProductMiddleware.update({ shop, accessToken, product });
});

AdminApi.delete('/products', bodyParser(), async (ctx) => {
  const { shop, accessToken } = ctx.state;
  const { id } = ctx.request.body;
  ctx.body = await ProductMiddleware.delete({ shop, accessToken, id });
});
/**
 * ================================================
 */

/**
 * ================================================
 * START PRODUCTS GRAPHQL
 */
AdminApi.get('/graphql-products', async (ctx) => {
  const { shop, accessToken } = ctx.state;
  const { id, limit, nextPageInfo, previousPageInfo, search, vendor } = ctx.request.query;
  ctx.body = await ProductGraphqlMiddleware.find({
    shop,
    accessToken,
    id,
    limit,
    nextPageInfo,
    previousPageInfo,
    search,
    vendor,
  });
});
/**
 * ================================================
 */

/**
 * ================================================
 * ORDER APIS
 */
AdminApi.get('/orders', async (ctx) => {
  const { shop, accessToken } = ctx.state;
  const {} = ctx.request.query;
  ctx.body = await OrderMiddleware.find({
    shop,
    accessToken,
  });
});
/**
 * ================================================
 */

module.exports = AdminApi;
