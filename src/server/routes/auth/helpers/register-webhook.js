const rp = require('request-promise')
const { SHOPIFY_APP_WEBHOOK, API_VER } = process.env

const registerWebhook = (shop, accessToken, webhooks) => {
  return new Promise((resolve, reject) => {
    let taskCount = webhooks.length
    for (let i = 0; i < webhooks.length; i++) {
      setTimeout(() => {
        let payload = {
          webhook: {
            topic: webhooks[i],
            address: SHOPIFY_APP_WEBHOOK,
            format: 'json',
          },
        }

        rp({
          uri: `https://${shop}/admin/api/${API_VER}/webhooks.json`,
          method: 'POST',
          json: true,
          headers: {
            'X-Shopify-Access-Token': accessToken,
            'Content-type': 'application/json; charset=utf-8',
          },
          body: payload,
        })
          .then((response) => {
            console.log(`webhook ${webhooks[i]} registered`)
          })
          .catch((err) => {})
          .finally(() => {
            taskCount--
            if (!taskCount) {
              resolve(1)
            }
          })
      }, i * 500)
    }
  })
}

module.exports = registerWebhook
