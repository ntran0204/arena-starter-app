const Router = require('koa-router')
const AppApi = new Router()
const AppStatus = require('../middlewares/app-status')

const getAppToken = async (ctx, next) => {
  try {
    const storeInfo = await AppStatus.GetStore(ctx.query.shop)

    if (storeInfo.status === 'error') {
      throw new Error(storeInfo.msg)
    }

    if (storeInfo.store.status !== 'installed' && storeInfo.store.status !== 'running') {
      throw new Error('App still not installed')
    }

    if (!storeInfo.store.token) {
      throw new Error('Invalid app token')
    }

    ctx.state.shop = ctx.query.shop
    ctx.state.accessToken = storeInfo.store.token

    return next()
  } catch (err) {
    ctx.body = {
      status: 'error',
      msg: err.message,
    }
    return
  }
}

const setCors = async (ctx, next) => {
  const { accessToken } = ctx.state
  if (accessToken) {
    ctx.set('Access-Control-Allow-Credentials', 'true')
    ctx.set('Access-Control-Allow-Origin', '*')

    return next()
  } else {
    ctx.body = {
      status: 'error',
      msg: 'Invalid app token',
    }
    return
  }
}

AppApi.use(getAppToken)
AppApi.use(setCors)

module.exports = AppApi
