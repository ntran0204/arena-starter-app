const { postgresql } = require('../connector/postgresql')
const ResponseHandler = require('../utils/responseHandler')

const toJson = (storeSetting) => {
  delete storeSetting.token

  return storeSetting
}

const find = async ({ shop }) => {
  try {
    let res = await postgresql.query(`select * from store_settings where store_name = '${shop}'`)
    if (res.rowCount) {
      return ResponseHandler.success(toJson(res.rows[0]))
    } else {
      throw { message: 'Invalid shop session' }
    }
  } catch (error) {
    console.log(`StoreSettingMiddleware.find error`, error)
    return ResponseHandler.error(error)
  }
}

const update = async ({ shop, accepted_date }) => {
  try {
    let updateStr = ``

    if (accepted_date) {
      updateStr = `accepted_date = now(), status = 'running'`
    }

    if (updateStr) {
      let res = await postgresql.query(
        `update store_settings set ${updateStr} where store_name = '${shop}' returning *;`,
      )
      if (res.rowCount) {
        return ResponseHandler.success(toJson(res.rows[0]))
      } else {
        throw { message: 'Update store setting failed' }
      }
    } else {
      throw { message: 'Bad request' }
    }
  } catch (error) {
    console.log(`StoreSettingMiddleware.update error`, error)
    return ResponseHandler.error(error)
  }
}

const uninstall = async ({ shop }) => {
  try {
    let res = await postgresql.query(
      `update store_settings 
       set status = 'uninstalled', uninstall_date = now(), install_date = null, accepted_date = null
       where store_name = '${shop}'
       returning *;`,
    )
    if (res.rowCount) {
      return ResponseHandler.success(toJson(res.rows[0]))
    } else {
      throw { message: 'Update store setting failed' }
    }
  } catch (error) {
    console.log(`StoreSettingMiddleware.uninstall error`, error)
    return ResponseHandler.error(error)
  }
}

const StoreSettingMiddleware = {
  toJson,
  find,
  update,
  uninstall,
}

module.exports = StoreSettingMiddleware
