const { apiCaller } = require('../utils/apiCaller');
const ResponseHandler = require('../utils/responseHandler');
const { API_VER } = process.env;

const LIMIT_PER_REQUEST = 250;

const find = async ({ shop, accessToken }) => {
  try {
    let res = await apiCaller({
      shop,
      accessToken,
      endpoint: `/admin/api/${API_VER}/orders.json`,
      method: 'GET',
      pageInfo: true,
    });

    if (res.success) {
      return ResponseHandler.success(res.payload);
    } else {
      throw res.error;
    }
  } catch (error) {
    console.log(`OrderMiddleware.find error`, error);
    return ResponseHandler.error(error);
  }
};

const OrderMiddleware = {
  find,
};

module.exports = OrderMiddleware;
