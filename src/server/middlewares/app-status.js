const { postgresql } = require('../connector/postgresql')

module.exports = {
  /**
   * FUNCTION TO GET STORE INFO TO CHECK INSTALLING STATUS
   */
  GetStore: async (shop) => {
    try {
      let res = await postgresql.query(
        `SELECT store_name, status, token FROM store_settings WHERE store_name='${shop}'`
      )

      return {
        status: 'success',
        store: res.rows[0],
      }
    } catch (error) {
      console.log(`AppStatus.GetStore error`, error)
      return {
        status: 'error',
        msg: error.message,
      }
    }
  },

  /**
   * FUNCTION TO LOAD NONCE VALUE FOR INSTALLATION APP  PROCESS
   */
  GetNonce: async (shop) => {
    try {
      let res = await postgresql.query(
        `SELECT nonce FROM store_settings WHERE store_name = '${shop}'`
      )
      if (res.rowCount) {
        return {
          status: 'success',
          nonce: res.rows[0].nonce,
        }
      } else {
        throw { message: 'Cannot get nonce value' }
      }
    } catch (error) {
      console.log(`AppStatus.GetNonce error`, error)
      return {
        status: 'error',
        msg: error.message,
      }
    }
  },

  /**
   * FUNCTION TO SAVE NONCE VALUE FOR INSTALLATION APP PROCESS
   */
  SetNonce: async (shop, nonce) => {
    try {
      let res = await postgresql.query(`SELECT * FROM store_settings WHERE store_name='${shop}'`)
      if (res.rowCount) {
        // Update store setting
        res = await postgresql.query(
          `UPDATE store_settings SET status = 'installing', nonce = '${nonce}' WHERE store_name='${shop}' RETURNING *;`
        )
        if (!res.rowCount) {
          throw { message: 'Cannot set nonce value' }
        } else {
          console.log(`App re-installed (${shop})`)
        }
      } else {
        // Create store setting
        res = await postgresql.query(
          `INSERT INTO store_settings (store_name, status, nonce) VALUES ('${shop}', 'installing', '${nonce}')`
        )
        if (!res.rowCount) {
          throw { message: 'Cannot init store setting' }
        } else {
          console.log(`App installed (${shop})`)
        }
      }

      return {
        status: 'success',
      }
    } catch (error) {
      console.log(`AppStatus.SetNonce error`, error)
      return {
        status: 'error',
        msg: error.message,
      }
    }
  },
}
