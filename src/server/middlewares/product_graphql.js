const graphqlCaller = require('../utils/graphqlCaller')
const ResponseHandler = require('../utils/responseHandler')

const LIMIT_PER_REQUEST = 250

const find = async ({
  shop,
  accessToken,
  id,
  limit,
  nextPageInfo,
  previousPageInfo,
  search,
  vendor,
}) => {
  try {
    if (id) {
      const query = `
        {
          product(id: "${id}") {
            id
            title
            description
            featuredImage {
              originalSrc
            }
            productType
            vendor
            options {
              id
            }
            variants (first: 10) {
              edges {
                node {
                  id
                }
              }
            }
            onlineStorePreviewUrl 
            status
            handle
          }
        }`

      return await graphqlCaller(shop, accessToken, query)
    } else {
      // set default value
      let _limit = parseInt(limit)
      if (!_limit || _limit > LIMIT_PER_REQUEST || _limit < 1) {
        _limit = LIMIT_PER_REQUEST
      }

      let searchStr = ``
      if (search || vendor) {
        searchStr += `, query: "${search ? `title:*${search}*` : ''}${
          search && vendor ? ` AND ` : ''
        }${vendor ? `vendor:${vendor}` : ''}"`
      }

      let page_info = ``
      if (nextPageInfo) {
        page_info = `, first: ${_limit}, after: "${nextPageInfo}"`
      }
      if (previousPageInfo) {
        page_info = `, last: ${_limit}, before: "${previousPageInfo}"`
      }
      if (!page_info) {
        page_info = `, first: ${_limit}`
      }

      const query = `
        {
          products(sortKey: TITLE${page_info}${searchStr}) {
            edges {
              node {
                id
                title
                description
                featuredImage {
                  originalSrc
                }
                productType
                vendor
                options {
                  id
                }
                variants (first: 10) {
                  edges {
                    node {
                      id
                    }
                  }
                }
                onlineStorePreviewUrl 
                status
                handle
              }
              cursor
            }
            pageInfo {
              hasNextPage
              hasPreviousPage
            }
          } 
        }`

      let res = await graphqlCaller(shop, accessToken, query)
      if (res.success) {
        let data = res.payload.data.products
        let payload = {
          items: data.edges.map((item) => item.node),
          pageInfo: {
            hasNextPage: data.pageInfo.hasNextPage,
            hasPreviousPage: data.pageInfo.hasPreviousPage,
            nextPageInfo: data.pageInfo.hasNextPage ? data.edges[data.edges.length - 1].cursor : '',
            previousPageInfo: data.pageInfo.hasNextPage ? data.edges[0].cursor : '',
          },
        }

        return ResponseHandler.success(payload)
      } else {
        throw res.error
      }
    }
  } catch (error) {
    console.log(`ProductGraphqlMiddleware.find error`, error)
    return ResponseHandler.error(error)
  }
}

const ProductGraphqlMiddleware = {
  find,
}

module.exports = ProductGraphqlMiddleware
