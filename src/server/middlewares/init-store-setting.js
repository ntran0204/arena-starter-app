const { GetStoreContact } = require('./get-store-contact')
const { postgresql } = require('../connector/postgresql')

module.exports.InitStoreSetting = async ({ shop, accessToken, scopes }) => {
  try {
    let contact = await GetStoreContact({ shop, accessToken })

    let storeSetting = null
    let storeSettingRes = await postgresql.query(
      `SELECT * FROM store_settings WHERE store_name = '${shop}'`
    )
    if (storeSettingRes.rowCount) {
      storeSetting = storeSettingRes.rows[0]
    }

    if (!storeSetting) {
      // App install the first time
      // Init new store setting
      let createdRes = await postgresql.query(
        `INSERT INTO store_settings (store_name, status, install_date, contact, store_plan, app_plan, accepted_date, scopes, nonce, token, uninstall_date) 
        VALUES ('${shop}', 'installed', now(), '${contact.email}', '${contact.plan_name}', 'free', null, '${scopes}', null, '${accessToken}', null);`
      )
      storeSetting = createdRes.rows[0]
      console.log(`Store setting updated for install (${shop})`)
    } else {
      // Store re-install app
      // Update store settings
      let updatedRes = await postgresql.query(
        `UPDATE store_settings 
        SET status = 'installed', install_date = now(), contact = '${contact.email}', store_plan = '${contact.plan_name}', app_plan = 'free', accepted_date = null, scopes = '${scopes}', nonce = null, token = '${accessToken}', uninstall_date = null
        WHERE store_name = '${shop}'
        RETURNING *;`
      )
      storeSetting = updatedRes.rows[0]
      console.log(`Store setting updated for re-install (${shop})`)
    }

    return {
      status: 'success',
      msg: 'Init store setting to db successfully',
      contact,
    }
  } catch (error) {
    console.log(`InitStoreSetting error`, error)
    return {
      status: 'error',
      msg: error.message,
    }
  }
}
