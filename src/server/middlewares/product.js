const { apiCaller } = require('../utils/apiCaller')
const ResponseHandler = require('../utils/responseHandler')
const { API_VER } = process.env

const LIMIT_PER_REQUEST = 250

const find = async ({ shop, accessToken, limit, pageInfo, search }) => {
  try {
    // default params
    let _limit = parseInt(limit)
    if (!_limit || _limit > LIMIT_PER_REQUEST || _limit < 1) {
      _limit = LIMIT_PER_REQUEST
    }
    let _search = search ? `&title=${search}` : ``
    let _pageInfo = pageInfo ? `&page_info=${pageInfo}` : ``

    let res = await apiCaller({
      shop,
      accessToken,
      endpoint: `/admin/api/${API_VER}/products.json?limit=${_limit}${_pageInfo}${_search}`,
      method: 'GET',
      pageInfo: true,
    })

    if (res.success) {
      return ResponseHandler.success(res.payload)
    } else {
      throw res.error
    }
  } catch (error) {
    console.log(`ProductMiddleware.find error`, error)
    return ResponseHandler.error(error)
  }
}

const findById = async ({ shop, accessToken, id }) => {
  try {
    // validate params
    let paramObj = { id }
    let paramKeys = Object.keys(paramObj)
    for (let i = 0, leng = paramKeys.length; i < leng; i++) {
      if (!paramObj[paramKeys[i]]) {
        throw { message: 'Field cannot be blank', field: paramKeys[i] }
      }
    }

    return await apiCaller({
      shop,
      accessToken,
      endpoint: `/admin/api/${API_VER}/products/${id}.json`,
      method: 'GET',
    })
  } catch (error) {
    console.log(`ProductMiddleware.findById error`, error)
    return ResponseHandler.error(error)
  }
}

const getCount = async ({ shop, accessToken }) => {
  try {
    return await apiCaller({
      shop,
      accessToken,
      endpoint: `/admin/api/${API_VER}/products/count.json`,
      method: 'GET',
    })
  } catch (error) {
    console.log(`ProductMiddleware.getCount error`, error)
    return ResponseHandler.error(error)
  }
}

const create = async ({ shop, accessToken, product }) => {
  try {
    // validate params
    let paramObj = { product }
    let paramKeys = Object.keys(paramObj)
    for (let i = 0, leng = paramKeys.length; i < leng; i++) {
      if (!paramObj[paramKeys[i]]) {
        throw { message: 'Field cannot be blank', field: paramKeys[i] }
      }
    }

    // validate body params
    const { title, body_html } = product
    let bodyObj = { title }
    let bodyKeys = Object.keys(bodyObj)
    for (let i = 0, leng = bodyKeys.length; i < leng; i++) {
      if (!bodyObj[bodyKeys[i]]) {
        throw { message: `Field "${bodyKeys[i]}" cannot be blank` }
      }
    }

    return await apiCaller({
      shop,
      accessToken,
      endpoint: `/admin/api/${API_VER}/products.json`,
      method: 'POST',
      data: { product },
    })
  } catch (error) {
    console.log(`ProductMiddleware.create error`, error)
    return ResponseHandler.error(error)
  }
}

const update = async ({ shop, accessToken, product }) => {
  try {
    // validate params
    let paramObj = { product }
    let paramKeys = Object.keys(paramObj)
    for (let i = 0, leng = paramKeys.length; i < leng; i++) {
      if (!paramObj[paramKeys[i]]) {
        throw { message: 'Field cannot be blank', field: paramKeys[i] }
      }
    }

    // validate body params
    const { id } = product
    let bodyObj = { id }
    let bodyKeys = Object.keys(bodyObj)
    for (let i = 0, leng = bodyKeys.length; i < leng; i++) {
      if (!bodyObj[bodyKeys[i]]) {
        throw { message: `Field "${bodyKeys[i]}" cannot be blank` }
      }
    }

    return await apiCaller({
      shop,
      accessToken,
      endpoint: `/admin/api/${API_VER}/products/${id}.json`,
      method: 'PUT',
      data: { product },
    })
  } catch (error) {
    console.log(`ProductMiddleware.update error`, error)
    return ResponseHandler.error(error)
  }
}

const _delete = async ({ shop, accessToken, id }) => {
  try {
    // validate params
    let paramObj = { id }
    let paramKeys = Object.keys(paramObj)
    for (let i = 0, leng = paramKeys.length; i < leng; i++) {
      if (!paramObj[paramKeys[i]]) {
        throw { message: 'Field cannot be blank', field: paramKeys[i] }
      }
    }

    return await apiCaller({
      shop,
      accessToken,
      endpoint: `/admin/api/${API_VER}/products/${id}.json`,
      method: 'DELETE',
    })
  } catch (error) {
    console.log(`ProductMiddleware._delete error`, error)
    return ResponseHandler.error(error)
  }
}

const ProductMiddleware = {
  find,
  findById,
  getCount,
  create,
  update,
  delete: _delete,
}

module.exports = ProductMiddleware
