const { Pool } = require('pg')
const { POSTGRES_USER, POSTGRES_HOST, POSTGRES_DB, POSTGRES_PWD, POSTGRES_PORT } = process.env

const pgConfig = {
  user: POSTGRES_USER,
  host: POSTGRES_HOST,
  database: POSTGRES_DB,
  password: POSTGRES_PWD,
  port: POSTGRES_PORT,
}
const pool = new Pool(pgConfig)

/**
 *
 * @param {String} queryStr
 * @returns Object
 */
const query = async (queryStr) => {
  const client = await pool.connect()
  try {
    return await client.query(queryStr)
  } catch (error) {
    throw error
  } finally {
    client.release()
  }
}

module.exports.postgresql = {
  query,
}
